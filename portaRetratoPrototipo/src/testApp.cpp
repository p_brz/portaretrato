#include "testApp.h"

#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>


#include <sys/param.h>//MAXPATHLEN
#include <magic.h>// obter mimeType -- exige instalar pacote libmagic-dev

#include <sys/types.h>
#include <sys/stat.h>

#include "ofxCvHaarFinder.h"

using namespace std;

/*function... might want it in some class?*/
int getDirFilenames (string baseDir, vector<string> &files)
{
    DIR *dp;
    struct dirent * dirIterator;
    if((dp  = opendir(baseDir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << baseDir << endl;
        return errno;
    }

    while ((dirIterator = readdir(dp)) != NULL) {
    	//TODO: checar se baseDir termina com '/'
        files.push_back(baseDir + "/" + string(dirIterator->d_name));
    }
    closedir(dp);
    return 0;
}

//string getMimeType(const char * filename){
//	string mimeType;
//
//	magic_t magic_cookie;
//	/*MAGIC_MIME tells magic to return a mime of the file, but you can specify different things*/
//	magic_cookie = magic_open(MAGIC_MIME);
//
//	if(magic_cookie != NULL){
////		printf("Loading default magic database\n");
////		if (magic_check(magic_cookie, filename) == 0 && magic_load(magic_cookie, NULL) == 0) {
////			const char *magic_full = magic_file(magic_cookie, filename);
////			mimeType = string(magic_full != NULL ? magic_full : "?");
////
////			cout << "Magic_full: " << magic_full << endl;
////
////		}
//
////		cout << "magic_check " << filename << " : " << magic_check(magic_cookie, filename) << endl;
//
//#ifdef MAGIC
//		cout << "MAGIC: " << MAGIC <<endl;
//		int err = magic_load(magic_cookie, NULL);
//#else
//		int err = magic_load(magic_cookie, "/usr/share/misc/magic.mgc");
//#endif
//		cout<< "magi_load err: " << err << endl;
//		if(err==0){
//			if(magic_errno(magic_cookie) != 0){
//				cout << "magic error: " << magic_error(magic_cookie) << endl;
//			}
//			if(magic_check(magic_cookie, filename) == -1){
//				cout << magic_error(magic_cookie) << endl;
//			}
//		}
//		magic_close(magic_cookie);
//	}
//
//
//	return mimeType;
//}

//--------------------------------------------------------------
string getFileExt(const string& s) {
	size_t extIndex = s.rfind('.', s.length( ));
	if (extIndex != string::npos) {
		return(s.substr(extIndex+1, s.length( ) - extIndex));
	}
	return("");
}

bool isDirectory(const char * filename){
	struct stat fileInfo;
	stat(filename, &fileInfo);

	return (fileInfo.st_mode & S_IFMT) == S_IFDIR;
}



void filterOnlyFiles(vector<string> & files){
	cout << "all files & directories: " <<endl;
	for(unsigned int i=0; i < files.size(); ++i){
		cout << "\t"<< files[i] << endl;
		if(isDirectory(files[i].c_str())){
			files.erase(files.begin() + i);
			--i; //apagou um indice
		}
	}
	cout << "only files: " <<endl;
	for(unsigned int i=0; i < files.size(); ++i){
		cout << "\t"<< files[i] << endl;
	}
}

string getCwd(){
	char cwd[MAXPATHLEN];
	getcwd(cwd, MAXPATHLEN);
	return string(cwd);
}

int loadSomeImage(ofImage & image, vector<string> & imgsFilename, unsigned int startIndex =0){
	unsigned int imageIndex = startIndex;
	while(imageIndex < imgsFilename.size()
			&& image.loadImage(imgsFilename[imageIndex]) == 0 ){
		cerr << "could not load: " << imgsFilename[imageIndex] << endl;
		imgsFilename.erase(imgsFilename.begin() + imageIndex);

		imageIndex = (imageIndex) % imgsFilename.size();
	}

	return imageIndex;
}

vector<string> imgFiles;
ofImage someImage;
int currentImageIndex;

ofxCvHaarFinder finder;

void testApp::setup(){
	finder.setup(getCwd() + "/data/haarcascade_frontalface_alt.xml");
	int errNum = getDirFilenames((getCwd() + string("/images")), imgFiles);

	if(errNum == 0){
		filterOnlyFiles(imgFiles);
	}
	else{
		ofLog(OF_LOG_ERROR) << "Error: " << errNum << endl;
		exitApp();
	}

	loadSomeImage(someImage, imgFiles);

	finder.findHaarObjects(someImage);

//	string currentPath = getCwd() + string("/images/");

//	for(unsigned int i=0; i < imgFiles.size(); ++i){
//		if(getFileExt(imgFiles[i]) == "jpg"){
////			cout<< imgFiles[i] << " mime type: " << getMimeType((string("./images/") + imgFiles[i]).c_str()) << endl;
//			cout<< imgFiles[i] << endl;
//			string fullpath = (currentPath + imgFiles[i]);
////			getMimeType(fullpath.c_str());
//			if(someImage.loadImage(fullpath)){
//				break;//ugly
//			}
//			else{
//				ofLog(OF_LOG_WARNING)<< "Cannot load image " << imgFiles[i] << endl;
//			}
//		}
//
//	}
}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){
	someImage.draw(0,0);

	ofNoFill();
	for(unsigned int i = 0; i < finder.blobs.size(); i++) {
		ofRectangle cur = finder.blobs[i].boundingRect;
		ofRect(cur.x, cur.y, cur.width, cur.height);
	}
}

void changeCurrentImage(int index){
	currentImageIndex = loadSomeImage(someImage, imgFiles, index);

	finder.findHaarObjects(someImage);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	if(key == OF_KEY_LEFT){
		changeCurrentImage((currentImageIndex - 1) % imgFiles.size());
	}
	else if(key == OF_KEY_RIGHT){
		changeCurrentImage((currentImageIndex + 1) % imgFiles.size());
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
