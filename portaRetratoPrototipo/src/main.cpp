#include "ofMain.h"
#include "testApp.h"

#include <iostream>
using namespace std;

#include <unistd.h>
#include <sys/param.h>
//========================================================================


int main(int argc, char * argv[] ){
	if(argc >= 1){
		cout<< argv[0] << endl;
	}
	char cwd[MAXPATHLEN];
	getcwd(cwd, MAXPATHLEN);
	cout << MAXPATHLEN << endl;
	cout<< cwd << endl;

	ofSetupOpenGL(1024,768,OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new testApp());

}

//#include "FaceDetectionCvApp.h"
//int main(int argc, char * argv[] ){
//	FaceDetectionCvApp app;
//
//	app.setup();
//
//	while(true)
//	{
//		app.update();
//		app.draw();
//	}
//
//}
